package com.exchangeratesassignment.exchangeratesassignment.ui.details;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.exchangeratesassignment.exchangeratesassignment.R;
import com.exchangeratesassignment.exchangeratesassignment.client.APIClient;
import com.exchangeratesassignment.exchangeratesassignment.client.APIInterface;
import com.exchangeratesassignment.exchangeratesassignment.model.BankInfo;
import com.exchangeratesassignment.exchangeratesassignment.model.Branch;
import com.exchangeratesassignment.exchangeratesassignment.model.BranchLocation;
import com.exchangeratesassignment.exchangeratesassignment.model.Rate;
import com.exchangeratesassignment.exchangeratesassignment.model.WorkHours;
import com.exchangeratesassignment.exchangeratesassignment.ui.BaseActivity;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class BankDetailsActivity extends BaseActivity {

    private static final String ORGANIZATION_ID = "extra.organizationId";
    private static final String BANK_NAME = "extra.bankName";
    private static final String LOGO = "extra.logo";
    private static final String RATES = "extra.rates";
    private static final String EXCHANGE_TYPE_POSITION = "extra.exchangeTypePosition";

    public static Intent makeIntent(Context context, String id, String bankName, String logo,
                                    HashMap<String, Map<String, Rate>> rates, int exchangeTypePosition) {
        Intent intent = new Intent(context, BankDetailsActivity.class);
        intent.putExtra(ORGANIZATION_ID, id);
        intent.putExtra(BANK_NAME, bankName);
        intent.putExtra(LOGO, logo);
        intent.putExtra(RATES, rates);
        intent.putExtra(EXCHANGE_TYPE_POSITION, exchangeTypePosition);
        return intent;
    }

    private ViewPager mViewPager;
    private BankPagerAdapter mBankPagerAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bank_details);

        mViewPager = findViewById(R.id.view_pager);

        requestRate();
    }

    private void requestRate() {
        final String key = getIntent().getStringExtra(ORGANIZATION_ID);
        final String bankName = getIntent().getStringExtra(BANK_NAME);
        final String logo = getIntent().getStringExtra(LOGO);
        final int exchangeTypePosition = getIntent().getIntExtra(EXCHANGE_TYPE_POSITION, 0);
        final HashMap<String, Map<String, Rate>> data = (HashMap<String, Map<String, Rate>>) getIntent().getSerializableExtra(RATES);

        final APIInterface apiInterface = APIClient.getInstance().create(APIInterface.class);
        apiInterface.getBank(key).enqueue(new Callback<BankInfo>() {
            @Override
            public void onResponse(@NonNull Call<BankInfo> call, @NonNull Response<BankInfo> response) {
                final BankInfo bankInfo = response.body();
                if (bankInfo != null) {
                    mBankPagerAdapter = new BankPagerAdapter(BankDetailsActivity.this, bankName, logo, bankInfo.getList(), data, exchangeTypePosition);
                    mViewPager.setAdapter(mBankPagerAdapter);
                }
            }

            @Override
            public void onFailure(@NonNull Call<BankInfo> call, @NonNull Throwable t) {

            }
        });
    }

    class BankPagerAdapter extends PagerAdapter {
        private static final String BACKGROUND = "https://images.unsplash.com/photo-1527956041665-d7a1b380c460?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1534&q=80";

        private final Context mContext;
        private final String mBankName;
        private final String mLogo;
        private final int mExchangeTypePosition;
        private final Map<String, Branch> mMap;
        private final HashMap<String, Map<String, Rate>> mData;


        BankPagerAdapter(Context context, String bankName, String logo, Map<String, Branch> map, HashMap<String, Map<String, Rate>> data, int exchangeTypePosition) {
            this.mContext = context;
            this.mBankName = bankName;
            this.mLogo = logo;
            this.mExchangeTypePosition = exchangeTypePosition;
            this.mMap = map;
            this.mData = data;
        }

        @NonNull
        @Override
        public Object instantiateItem(@NonNull ViewGroup container, int position) {
            View view = LayoutInflater.from(mContext).inflate(R.layout.branch_item, container, false);

            Picasso.get().load(Uri.parse(BACKGROUND)).into(view.<ImageView>findViewById(R.id.background_details));

            final Branch branch = (new ArrayList<>(mMap.values())).get(position);
            Picasso.get().load(mLogo).into(view.<CircleImageView>findViewById(R.id.logo_details));

            view.<TextView>findViewById(R.id.label_bank_name_details).setText(mBankName);

            view.<TextView>findViewById(R.id.label_bank_city_details).setText(branch.getTitle().getSelectedLanguage(mLanguage));

            view.<TextView>findViewById(R.id.label_bank_address_details).setText(branch.getAddress().getSelectedLanguage(mLanguage));

            TextView labelPhoneNumber = view.findViewById(R.id.label_phone_number_details);
            labelPhoneNumber.setText(branch.getContacts());

            TextView labelOpenBankTimeWorkingDays = view.findViewById(R.id.time_working_days_details);
            TextView labelOpenBankTimeWeekend = view.findViewById(R.id.time_weekend_details);
            TextView labelWeekendDetails = view.findViewById(R.id.data_weekend_details);
            for (WorkHours workHours : branch.getWorkHours()) {
                if (workHours.getDays().contains("1") || workHours.getDays().contains("5")) {
                    if (workHours.getHours().equals("24")) {
                        labelOpenBankTimeWorkingDays.setText(String.format("%s %s", workHours.getHours(), getResources().getString(R.string.time)));
                    } else {
                        labelOpenBankTimeWorkingDays.setText(workHours.getHours());
                    }
                } else if (workHours.getDays().contains("6")) {
                    labelWeekendDetails.setVisibility(View.VISIBLE);
                    labelOpenBankTimeWeekend.setText(workHours.getHours());
                }
            }

            RecyclerView recyclerView = view.findViewById(R.id.recycler_view_details);
            recyclerView.setAdapter(new BankAdapter(BankDetailsActivity.this, mData, mExchangeTypePosition));


            view.findViewById(R.id.view_on_map).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    BranchLocation loc = branch.getLocation();
                    Uri gmmIntentUri = Uri.parse(String.format("geo:%f,%f?q=%f,%f(%s)", loc.getLat(), loc.getLng(), loc.getLat(), loc.getLng(), mBankName));
                    Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
                    v.getContext().startActivity(mapIntent);
                }
            });

            container.addView(view);
            return view;
        }

        @Override
        public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object view) {
            container.removeView((View) view);
        }

        @Override
        public int getCount() {
            return mMap == null ? 0 : mMap.values().size();
        }

        @Override
        public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
            return object == view;
        }
    }
}
