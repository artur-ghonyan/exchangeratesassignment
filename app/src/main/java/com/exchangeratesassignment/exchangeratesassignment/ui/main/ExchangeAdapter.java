package com.exchangeratesassignment.exchangeratesassignment.ui.main;

import android.content.Context;
import android.location.Location;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.exchangeratesassignment.exchangeratesassignment.R;
import com.exchangeratesassignment.exchangeratesassignment.model.Bank;
import com.exchangeratesassignment.exchangeratesassignment.model.Rate;
import com.squareup.picasso.Picasso;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ExchangeAdapter extends RecyclerView.Adapter<ExchangeAdapter.ExchangeHolder> {
    private final BankClickListener mOnClickListener;
    private final LayoutInflater mInflater;
    private Map<String, Bank> mData;
    private String mSelectedExchange;
    private int mExchangeTypePosition;
    private Location mUserLocation;

    ExchangeAdapter(Context context, BankClickListener onClickListener, Map<String, Bank> data) {
        mOnClickListener = onClickListener;
        mData = data;
        mInflater = LayoutInflater.from(context);
    }

    void setUserLocation(Location userLocation) {
        mUserLocation = userLocation;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public ExchangeHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int position) {
        View view = mInflater.inflate(R.layout.exchange_item, viewGroup, false);
        return new ExchangeHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ExchangeHolder exchangeHolder, int position) {
        exchangeHolder.onBind(position);
    }

    @Override
    public int getItemCount() {
        return mData.size();
    }

    void setData(Map<String, Bank> data) {
        mData = data;
        notifyDataSetChanged();
    }

    void setSelectedExchange(String selectedExchange) {
        mSelectedExchange = selectedExchange;
        notifyDataSetChanged();
    }

    void setExchangeTypePosition(int exchangeTypePosition) {
        mExchangeTypePosition = exchangeTypePosition;
        notifyDataSetChanged();
    }

    class ExchangeHolder extends RecyclerView.ViewHolder {
        private final ImageView mImageViewLogo;
        private final TextView mLabelBankName;
        private final TextView mLabelDistance;
        private final TextView mLabelBuy;
        private final TextView mLabelSell;

        ExchangeHolder(@NonNull View itemView) {
            super(itemView);

            mImageViewLogo = itemView.findViewById(R.id.image_view_logo);
            mLabelBankName = itemView.findViewById(R.id.label_bank_name);
            mLabelDistance = itemView.findViewById(R.id.label_distance);
            mLabelBuy = itemView.findViewById(R.id.label_buy);
            mLabelSell = itemView.findViewById(R.id.label_sell);
        }

        void onBind(final int position) {
            final Bank bank = (new ArrayList<>(mData.values())).get(position);

            Picasso.get().load(bank.getLogo()).into(mImageViewLogo);
            mLabelBankName.setText(bank.getTitle());

            final Map<String, Rate> rateList = bank.getList().get(mSelectedExchange);
            if (rateList != null) {
                try {
                    Rate rate;
                    if ((new ArrayList<>(rateList.values())).get(mExchangeTypePosition) != null) {
                        rate = (new ArrayList<>(rateList.values())).get(mExchangeTypePosition);
                    } else {
                        rate = (new ArrayList<>(rateList.values())).get(0);
                    }
                    if (rate != null) {
                        mLabelBuy.setText(String.valueOf(rate.getBuy()));
                        mLabelSell.setText(String.valueOf(rate.getSell()));
                    }
                } catch (Exception ignore) {
                }
            }

            if (mUserLocation != null) {
                if (bank.getBranchList() != null) {
                    List<Float> list = new ArrayList<>();
                    for (int i = 0; i < bank.getBranchList().size(); i++) {
                        double lat = bank.getBranchList().get(i).getLocation().getLat();
                        double lon = bank.getBranchList().get(i).getLocation().getLng();
                        list.add(calculateDistance(lat, lon));
                    }

                    if (list.size() > 0) {
                        Collections.sort(list);
                        BigDecimal bdDistance;
                        bdDistance = round(list.get(0) / 1000);
                        String strDistance = bdDistance.toString();
                        mLabelDistance.setText(mLabelDistance.getContext().getString(R.string.distance_format, strDistance));
                    }
                }
            }

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (bank.getList() != null) {
                        final String key = (new ArrayList<>(mData.keySet()).get(position));
                        mOnClickListener.onClick(key, bank.getTitle(), bank.getLogo(), new HashMap<>(bank.getList()));
                    }
                }
            });
        }
    }

    private BigDecimal round(float d) {
        BigDecimal bd = new BigDecimal(Float.toString(d));
        bd = bd.setScale(1, BigDecimal.ROUND_HALF_UP);
        return bd;
    }

    private float calculateDistance(double latBank, double lonBank) {
        Location locBank = new Location("");
        locBank.setLatitude(latBank);
        locBank.setLongitude(lonBank);

        return mUserLocation.distanceTo(locBank);
    }
}
