package com.exchangeratesassignment.exchangeratesassignment.ui;

import com.exchangeratesassignment.exchangeratesassignment.R;

public class Utils {
    private static final Utils INSTANCE = new Utils();

    private final int[] mCurrencyFlags;

    public static Utils getInstance() {
        return INSTANCE;
    }

    private Utils() {
        mCurrencyFlags = new int[]{
                R.drawable.usd,
                R.drawable.eur,
                R.drawable.rur,
                R.drawable.gel,
                R.drawable.gbp,
                R.drawable.chf,
                R.drawable.jpy,
                R.drawable.xau,
                R.drawable.aud,
                R.drawable.cad,
        };
    }

    public int[] getFlags() {
        return mCurrencyFlags;
    }
}
