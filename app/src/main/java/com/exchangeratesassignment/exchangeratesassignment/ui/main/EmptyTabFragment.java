package com.exchangeratesassignment.exchangeratesassignment.ui.main;

import android.os.Bundle;
import android.support.annotation.DrawableRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.exchangeratesassignment.exchangeratesassignment.R;

public class EmptyTabFragment extends Fragment {

    private static final String ARGUMENT_ICON = "arg.icon";
    private static final String ARGUMENT_TEXT = "arg.text";

    public static Fragment create(@DrawableRes int icon, @StringRes int text) {
        EmptyTabFragment fragment = new EmptyTabFragment();

        Bundle args = new Bundle();
        args.putInt(ARGUMENT_ICON, icon);
        args.putInt(ARGUMENT_TEXT, text);
        fragment.setArguments(args);

        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_empty_tab, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        if (getArguments() != null) {
            view.<ImageView>findViewById(R.id.image).setImageResource(getArguments().getInt(ARGUMENT_ICON));
            view.<TextView>findViewById(R.id.text).setText(getArguments().getInt(ARGUMENT_TEXT));
        }
    }
}
