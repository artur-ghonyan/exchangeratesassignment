package com.exchangeratesassignment.exchangeratesassignment.ui.main;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;

import com.exchangeratesassignment.exchangeratesassignment.R;
import com.exchangeratesassignment.exchangeratesassignment.ui.BaseActivity;

public class MainActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ViewPager viewPager = findViewById(R.id.view_pager);

        viewPager.setAdapter(new FragmentStatePagerAdapter(getSupportFragmentManager()) {
            @Override
            public Fragment getItem(int position) {
                switch (position) {
                    case 0:
                        return EmptyTabFragment.create(R.drawable.ic_currency_rates, R.string.transaction);

                    case 1:
                        return ExchangeFragment.create();

                    case 2:
                        return EmptyTabFragment.create(R.drawable.ic_calculator, R.string.calculator);

                    case 3:
                        return EmptyTabFragment.create(R.drawable.ic_menu, R.string.more);

                    default:
                        return null;
                }
            }

            @Nullable
            @Override
            public CharSequence getPageTitle(int position) {
                switch (position) {
                    case 0:
                        return getString(R.string.transaction);

                    case 1:
                        return getString(R.string.exchange_rates);

                    case 2:
                        return getString(R.string.calculator);

                    case 3:
                        return getString(R.string.more);

                    default:
                        return null;
                }
            }

            @Override
            public int getCount() {
                return 4;
            }
        });

        TabLayout tabLayout = findViewById(R.id.tab_layout);
        tabLayout.setupWithViewPager(viewPager);

        tabLayout.getTabAt(0).setIcon(R.drawable.ic_currency_rates);
        tabLayout.getTabAt(1).setIcon(R.drawable.ic_exchange);
        tabLayout.getTabAt(2).setIcon(R.drawable.ic_calculator);
        tabLayout.getTabAt(3).setIcon(R.drawable.ic_menu);

        viewPager.setCurrentItem(1);
    }
}
