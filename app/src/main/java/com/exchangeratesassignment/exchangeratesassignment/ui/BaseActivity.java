package com.exchangeratesassignment.exchangeratesassignment.ui;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.exchangeratesassignment.exchangeratesassignment.R;

public abstract class BaseActivity extends AppCompatActivity {
    protected String mLanguage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        init();
    }

    private void init() {
        String[] lang = getResources().getStringArray(R.array.language);
        mLanguage = lang[1];
    }
}
