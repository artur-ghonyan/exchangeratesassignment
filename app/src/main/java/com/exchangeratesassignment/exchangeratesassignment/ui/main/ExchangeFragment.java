package com.exchangeratesassignment.exchangeratesassignment.ui.main;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.Toast;

import com.exchangeratesassignment.exchangeratesassignment.R;
import com.exchangeratesassignment.exchangeratesassignment.client.APIClient;
import com.exchangeratesassignment.exchangeratesassignment.client.APIInterface;
import com.exchangeratesassignment.exchangeratesassignment.model.Bank;
import com.exchangeratesassignment.exchangeratesassignment.model.BankInfo;
import com.exchangeratesassignment.exchangeratesassignment.model.Branch;
import com.exchangeratesassignment.exchangeratesassignment.model.Rate;
import com.exchangeratesassignment.exchangeratesassignment.ui.Utils;
import com.exchangeratesassignment.exchangeratesassignment.ui.details.BankDetailsActivity;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnSuccessListener;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.Manifest.permission.ACCESS_COARSE_LOCATION;

public class ExchangeFragment extends Fragment implements BankClickListener {

    public static Fragment create() {
        return new ExchangeFragment();
    }

    private static final int PERMISSION_REQUEST_LOCATION = 1;

    private String mLanguage;

    private ExchangeAdapter mExchangeAdapter;
    private String[] mCurrencies;
    private int[] mCurrencyFlags;
    private FusedLocationProviderClient mLocationClient;
    private int mExchangeTypePosition;

    private ProgressBar mProgressBar;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        String[] lang = getResources().getStringArray(R.array.language);
        mLanguage = lang[1];

        mLocationClient = LocationServices.getFusedLocationProviderClient(requireContext());
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_exchange, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        Spinner mSpinnerExchange = view.findViewById(R.id.spinner_exchange_flag);
        mCurrencies = getResources().getStringArray(R.array.currencies);
        mCurrencyFlags = Utils.getInstance().getFlags();
        mSpinnerExchange.setAdapter(new ArrayAdapter<String>(requireContext(), R.layout.currency_item, R.id.name, Arrays.asList(mCurrencies)) {
            private View setFlag(int position, @NonNull View view) {
                view.<ImageView>findViewById(R.id.flag).setImageResource(mCurrencyFlags[position]);
                return view;
            }

            @NonNull
            @Override
            public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
                return setFlag(position, super.getView(position, convertView, parent));
            }

            @Override
            public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
                return setFlag(position, super.getDropDownView(position, convertView, parent));
            }
        });

        RecyclerView mRecyclerView = view.findViewById(R.id.recycler_view);
        mExchangeAdapter = new ExchangeAdapter(requireContext(), this, new HashMap<String, Bank>());
        mRecyclerView.setAdapter(mExchangeAdapter);

        mSpinnerExchange.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                mExchangeAdapter.setSelectedExchange(mCurrencies[position]);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        Spinner mSpinnerExchangeType = view.findViewById(R.id.spinner_exchange_type);
        mSpinnerExchangeType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                mExchangeTypePosition = position;
                mExchangeAdapter.setExchangeTypePosition(position);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        mProgressBar = view.findViewById(R.id.progress_bar);

        requestRate();
        requestLocation();
    }

    private void requestRate() {
        mProgressBar.setVisibility(View.VISIBLE);
        final APIInterface apiInterface = APIClient.getInstance().create(APIInterface.class);
        apiInterface.getExchange(mLanguage).enqueue(new Callback<Map<String, Bank>>() {
            @Override
            public void onResponse(@NonNull Call<Map<String, Bank>> call, @NonNull Response<Map<String, Bank>> response) {
                Map<String, Bank> map = response.body();
                if (map != null) {
                    requestBranches(map);
                }
            }

            @Override
            public void onFailure(@NonNull Call<Map<String, Bank>> call, @NonNull Throwable t) {
                mProgressBar.setVisibility(View.GONE);
                Toast.makeText(requireActivity(), "Error", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void requestBranches(final Map<String, Bank> map) {
        final APIInterface apiInterface = APIClient.getInstance().create(APIInterface.class);
        for (Map.Entry<String, Bank> entry : map.entrySet()) {
            final String id = entry.getKey();

            apiInterface.getBank(id).enqueue(new Callback<BankInfo>() {
                @Override
                public void onResponse(@NonNull Call<BankInfo> call, @NonNull Response<BankInfo> response) {
                    final BankInfo bankInfo = response.body();
                    if (bankInfo != null) {
                        List<Branch> branchList = new ArrayList<>();
                        for (int i = 0; i < bankInfo.getList().size(); i++) {
                            final Branch branch = (new ArrayList<>(bankInfo.getList().values())).get(i);
                            branchList.add(branch);
                        }
                        map.get(id).setBranchList(branchList);
                        mExchangeAdapter.setData(map);
                    }
                    mProgressBar.setVisibility(View.GONE);
                }

                @Override
                public void onFailure(@NonNull Call<BankInfo> call, @NonNull Throwable t) {
                    mProgressBar.setVisibility(View.GONE);
                    Toast.makeText(requireContext(), "Error", Toast.LENGTH_SHORT).show();
                }
            });
        }
    }

    private void requestLocation() {
        if (ContextCompat.checkSelfPermission(requireContext(), ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            requestPermissions(new String[]{ACCESS_COARSE_LOCATION}, PERMISSION_REQUEST_LOCATION);
            return;
        }

        mLocationClient.getLastLocation()
                .addOnSuccessListener(requireActivity(), new OnSuccessListener<Location>() {
                    @Override
                    public void onSuccess(Location location) {
                        if (location != null) {
                            mExchangeAdapter.setUserLocation(location);
                        }
                    }
                });
    }

    @Override
    public void onClick(String id, String bankName, String logo, HashMap<String, Map<String, Rate>> rates) {
        Intent intent = BankDetailsActivity.makeIntent(requireContext(), id, bankName, logo, rates, mExchangeTypePosition);
        startActivity(intent);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == PERMISSION_REQUEST_LOCATION) {
            if (grantResults.length > 0
                    && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                requestLocation();
            }
        } else {
            super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }
}
