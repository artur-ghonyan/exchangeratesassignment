package com.exchangeratesassignment.exchangeratesassignment.ui.details;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.exchangeratesassignment.exchangeratesassignment.R;
import com.exchangeratesassignment.exchangeratesassignment.model.Rate;
import com.exchangeratesassignment.exchangeratesassignment.ui.Utils;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import de.hdodenhof.circleimageview.CircleImageView;

public class BankAdapter extends RecyclerView.Adapter<BankAdapter.ExchangeHolder> {

    private final HashMap<String, Map<String, Rate>> mData;
    private final LayoutInflater mInflater;
    private final String[] mCurrencies;
    private final int[] mCurrencyFlags;
    private final int mExchangeTypePosition;

    BankAdapter(Context context, HashMap<String, Map<String, Rate>> data, int exchangeTypePosition) {
        mData = data;
        mInflater = LayoutInflater.from(context);
        mCurrencies = context.getResources().getStringArray(R.array.currencies);
        mCurrencyFlags = Utils.getInstance().getFlags();
        mExchangeTypePosition = exchangeTypePosition;
    }

    @NonNull
    @Override
    public ExchangeHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = mInflater.inflate(R.layout.currency_list_item, viewGroup, false);
        return new ExchangeHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ExchangeHolder exchangeHolder, int position) {
        exchangeHolder.onBind(position);
    }

    @Override
    public int getItemCount() {
        return mData.size();
    }

    class ExchangeHolder extends RecyclerView.ViewHolder {
        private final CircleImageView mImageViewFlag;
        private final TextView mLabelCurrency;
        private final TextView mLabelBuy;
        private final TextView mLabelSell;

        ExchangeHolder(@NonNull View itemView) {
            super(itemView);
            mImageViewFlag = itemView.findViewById(R.id.image_view_flag);
            mLabelCurrency = itemView.findViewById(R.id.label_currency);
            mLabelBuy = itemView.findViewById(R.id.label_buy);
            mLabelSell = itemView.findViewById(R.id.label_sell);
        }

        void onBind(final int position) {
            final Map<String, Rate> rateList = (new ArrayList<>(mData.values())).get(position);

            if (rateList != null) {
                try {
                    Rate rate;
                    if ((new ArrayList<>(rateList.values())).get(mExchangeTypePosition) != null) {
                        rate = (new ArrayList<>(rateList.values())).get(mExchangeTypePosition);
                    } else {
                        rate = (new ArrayList<>(rateList.values())).get(0);
                    }
                    if (rate != null) {
                        mLabelBuy.setText(String.valueOf(rate.getBuy()));
                        mLabelSell.setText(String.valueOf(rate.getSell()));
                    }
                } catch (Exception ignore) {
                }
            }

            final String name = (new ArrayList<>(mData.keySet())).get(position);
            mLabelCurrency.setText(name);

            for (int i = 0; i < mCurrencies.length; i++) {
                if (name.equals(mCurrencies[i])) {
                    Picasso.get().load(mCurrencyFlags[i]).into(mImageViewFlag);
                    break;
                }
            }
        }
    }
}
