package com.exchangeratesassignment.exchangeratesassignment.ui.main;

import com.exchangeratesassignment.exchangeratesassignment.model.Rate;

import java.util.HashMap;
import java.util.Map;

interface BankClickListener {

    void onClick(String id, String bankName, String logo, HashMap<String, Map<String, Rate>> rates);
}
