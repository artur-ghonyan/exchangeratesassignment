package com.exchangeratesassignment.exchangeratesassignment.model;

public class Information {
    private String en;
    private String am;
    private String ru;

    public String getSelectedLanguage(String language) {
        if (language.equals(en)) {
            return en;
        }
        if (language.equals(ru)) {
            return ru;
        }
        return am;
    }
}
