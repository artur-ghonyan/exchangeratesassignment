package com.exchangeratesassignment.exchangeratesassignment.model;

public class WorkHours {
    private String days;
    private String hours;

    public String getDays() {
        return days;
    }

    public String getHours() {
        return hours;
    }
}
