package com.exchangeratesassignment.exchangeratesassignment.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Branch {
    private int head;
    private Information title;
    private Information address;
    private BranchLocation location;
    private String contacts;
    @SerializedName("workhours")
    private List<WorkHours> workHours;

    public Information getTitle() {
        return title;
    }

    public Information getAddress() {
        return address;
    }

    public String getContacts() {
        return contacts;
    }

    public List<WorkHours> getWorkHours() {
        return workHours;
    }

    public BranchLocation getLocation() {
        return location;
    }
}
