package com.exchangeratesassignment.exchangeratesassignment.model;

public class BranchLocation {
    private double lat;
    private double lng;

    public double getLat() {
        return lat;
    }

    public double getLng() {
        return lng;
    }
}
