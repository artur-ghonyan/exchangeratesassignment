package com.exchangeratesassignment.exchangeratesassignment.model;

import java.util.List;
import java.util.Map;

public class Bank {
    private static final String LOGO_URL_FORMAT = "https://rate.am/images/organization/logo/%s";

    private String title;
    private Long date;
    private String logo;
    private Map<String, Map<String, Rate>> list;

    private List<Branch> branchList;

    public String getTitle() {
        return title;
    }

    public String getLogo() {
        return String.format(LOGO_URL_FORMAT, logo);
    }

    public Map<String, Map<String, Rate>> getList() {
        return list;
    }

    public List<Branch> getBranchList() {
        return branchList;
    }

    public void setBranchList(List<Branch> branchList) {
        this.branchList = branchList;
    }
}
