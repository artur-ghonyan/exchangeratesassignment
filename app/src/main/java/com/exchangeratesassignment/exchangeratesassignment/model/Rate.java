package com.exchangeratesassignment.exchangeratesassignment.model;

import java.io.Serializable;

public class Rate implements Serializable {
    private double buy;
    private double sell;

    public double getBuy() {
        return buy;
    }

    public double getSell() {
        return sell;
    }
}