package com.exchangeratesassignment.exchangeratesassignment.client;

import com.exchangeratesassignment.exchangeratesassignment.model.Bank;
import com.exchangeratesassignment.exchangeratesassignment.model.BankInfo;

import java.util.Map;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface APIInterface {

    @GET("rates.ashx")
    Call<Map<String, Bank>> getExchange(@Query("lang") String lang);

    @GET("branches.ashx")
    Call<BankInfo> getBank(@Query("id") String organizationId);
}